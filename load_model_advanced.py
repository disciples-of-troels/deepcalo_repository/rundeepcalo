import os
import sys
import json
import pickle
import joblib
import h5py
import numpy as np
import keras as ks
import copy
import pandas as pd
from os import listdir
from os.path import isfile, join
import concurrent.futures
import tensorflow as tf
import glob
sys.path.append('..')
import deepcalo as dpcal
sys.path.append('../invmfit')
from mc_performance import *
from data_performance import *
import argparse
from tqdm import tqdm
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
#%%
def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

def find_best_model_in_folder(mypath, folder, models, weights):
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    # print(onlyfiles)
    onlyfiles_model = np.array(onlyfiles)[np.array([i.endswith('json') for i in onlyfiles])]
    onlyfiles_weights = np.array(onlyfiles)[np.array([i.endswith('hdf5') for i in onlyfiles])]
    # onlyfiles = glob.glob(mypath+'*.json')
    best_model = np.array([float(i.split('-')[-1].split('.json')[0]) for i in onlyfiles_model]) 
    best_model[np.isnan(best_model)] = 100
    best_weights = np.array([float(i.split('-')[-1].split('.hdf5')[0]) for i in onlyfiles_weights]) 
    best_weights[np.isnan(best_weights)] = 100
    model = [onlyfiles_model[i] for i in np.argsort(best_model)[:nr_models]]
    weight = [onlyfiles_weights[i] for i in np.argsort(best_weights)[:nr_models]]
    # if len(folder) == 1:
    #     models = mypath+'/'+model[0]
    #     weights = mypath+'/'+weight[0]
    # else:
    models.append([mypath+'/'+i for i in model])
    weights.append([mypath+'/'+i for i in weight])
    return models, weights

if '__main__' == __name__:
    parser = argparse.ArgumentParser(description="Prediction using DeepCalo:")
    parser.add_argument('-g','--gpu', help='Which GPU(s) to use, e.g. "0" or "1,3".', default='-1', type=str)
    parser.add_argument('--data_path', help='data_params from train_model.py - not use manully!', default=None, type=str)
    parser.add_argument('--sub_data_folder_name', help='Which sub folder the data are within data_path', default=None, type=str)
    parser.add_argument('--gpu_dir', help='Path to which gpu dir the model(s) are saved within', default=None, type=str)
    parser.add_argument('--model_path', help='Folder name of the model(s)', default=None, nargs='+')
    args = parser.parse_args()
    print(args)
    gpu_ids = args.gpu
    data_path = args.data_path
    sub_data_folder_name = args.sub_data_folder_name
    gpu_dir = args.gpu_dir
    folder = args.model_path
    print(folder) 
    os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu_ids) # -1 disable
    os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
    print(os.environ["CUDA_VISIBLE_DEVICES"])
    print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))
    verbose = 0
    # Argument parsing
    if (data_path is None) & (sub_data_folder_name is None):
        folder = [
                    # 'Zee_single_mc_2021-09-10T11:25:00.171313_1000_epochs_Zee_single_electron_full_model',
                    # 'Zmumugam_mc_2021-09-04T12:38:04.009087_1000_epochs_Zmumugam_full_model'
                    # 'Zee_single_mc_2021-09-08T11:47:03.035450_1000_epochs_Zee_single_electron_full_model'
                    'Zee_mc_2021-09-01T15:55:42.210181_1000_epochs_Zee_histogram_eq'
                    # 'Zee_mc_2021-08-31T16:01:21.493125_1000_epochs_Zee_full_model'
                  ]

        #%%
        # gpu_dir = '0'
        # number of test points
        n_test = 1 # number of events in hdf5 
        nr_models = 1
        nr_files = 100000   
        # set_name=['test_data_classification', 'test_mc_classification']
        save=True
        set_name = 'Zee_test_data'
        # set_name = 'Zmumugam_test_mc'
        # set_name = 'Zee_single_test_mc'
        # set_name = 'Zee_test_data'
        # path_norm ='../tfrecords_data/Zee_mc/'

        # set_name = 'Hyy_test_mc'
        # path_norm ='../tfrecords_data/Hyy_mc/'
        ask_fit=True
        
    elif (data_path is not None) & (sub_data_folder_name is not None):
        # Running the load_model algorithm from train_model.py
        ask_fit = False
        gpu_dir = gpu_dir
        n_test = 1 
        nr_models=1
        nr_files = 100
        save=True
        set_name = sub_data_folder_name
        # path_norm = data_path
    else:
        print('Unknown inputs to load_model_advanced')
        sys.exit()
    #%%
    for gpu_dir in ['0', '0_1', '1', 'test_time_lr',
                    'performance_measures', 'benchmarks',
                    'testing_architecture',
                    'testing_architecture1',
                    'models_in_use']:
        try:
            hyperparameters = glob(f'./logs/{gpu_dir}/'+folder[0]+'/hyper*.pkl')[0]
            with open(hyperparameters, 'rb') as handle:
                print('GPU DIR:', gpu_dir)
                hyperparameters = pickle.load(handle)
            break
        except IndexError:
            print('No Hyperparameter.pkl file!')
            gpu_dir = None
    if gpu_dir == None:
        sys.exit()
    path_norm = hyperparameters['data_generator']['path'] # '../tfrecords_data/Zee_new_standardization_v1/'
        
    #%%
    if isinstance(set_name, list):
        path_data =[f'{path_norm}{i}/' for i in set_name]
        single_file = True
        isMC=True
    else:
        path_data =[f'{path_norm}/{set_name}/']
        single_file = 'h5' not in path_data
        isMC = not 'data' in '/'.join(path_data[0].split('/')[2:])
    #%%
    name = 'Zee' if 'Zee' in set_name else 'Hyy' if 'Hyy' in set_name else 'Zmumugam'  if 'Zmumugam' in set_name else 'unknown'
    tag = np.copy(name)
    if single_file:
        data_files=[]
        for path in path_data:
            files = [path+f for f in listdir(path) if isfile(join(path, f))]
            # sort_files = [int(i.split('_')[-2]) for i in data_files]
            # data_files = [x for _,x in sorted(zip(sort_files,data_files))]
            data_files.extend(files[:nr_files])
        data_type = 'h5' if 'h5' in data_files[0] else 'tfr'
        if isinstance(set_name, list):
            data_files = data_files[:6]
    #%%
    if False: # test file
        df = h5py.File(path_data, "r")
        # print(len(df['train']['p_e'][:]))
        print(len(df['test']['p_e'][:]))
        # print(len(df['val']['p_e'][:]))
        events = df['test']['eventNumber'][:100]
    
    #%%
    if folder != list:
        folder = list(folder)
    model_paths=[]
    weights_paths=[]
    all_data_params=[]
    for i in folder:
        params_dir = f'logs/{gpu_dir}/{i}'
        mypath = f'{params_dir}/saved_models/'
        multiple_model_folders = not any(['json' in i for i in glob(mypath+'*')])
        # load models and weights
        if multiple_model_folders:
            sub_model_folders = glob(mypath+'/*')
            for i in sub_model_folders:
                model_paths, weights_paths = find_best_model_in_folder(i, folder, model_paths, weights_paths)
            
        else:
            sub_model_folders = [1]
            model_paths, weights_paths = find_best_model_in_folder(mypath, folder, model_paths, weights_paths)
        # load data params
        with open(glob(params_dir+'/data*.pkl')[0], 'rb') as f:
            data_param = pickle.load(f)
        # data_param['target_name'] = 'p_truth_e'
        del data_param['flags']
        del data_param['data_path']
        del data_param['target_name']
        if not isinstance(data_param['gate_img_prefix'], list):
            data_param['gate_img_prefix'] = [data_param['gate_img_prefix']]
        data_param['additional_info'] = True
        all_data_params.append([data_param]*len(sub_model_folders)*nr_models)
    print('Models:', model_paths)
    model_paths = np.hstack(model_paths)
    weights_paths = np.hstack(weights_paths)
    all_data_params = np.hstack(all_data_params)
    # ======================================================
    # Load model
    # ======================================================
    # Model reconstruction from JSON
    # We need to include the custom FiLM layer, as implemented in DeepCalo.
    def load_model(*, model_path: str, weights_path: str, verbose=1):
        with open(model_path, 'r') as model_json:
            arch = json.load(model_json)
            model = ks.models.model_from_json(arch, custom_objects={'FiLM': dpcal.layers.FiLM()})
    
        if verbose:
            print(f'Model architecture loaded from {model_path}.')
            # Inspect the structure of the model
            model.summary()
    
        # Load weights
        model.load_weights(weights_path)
        if verbose:
            print(f'Weights loaded from {weights_path}.')
    
        return model
    
    def load_addisional_info(data, data_type, data_path, data_pileup, last_info=[]):
        global name
        if not (data_type == 'h5'):
        # =============================================================================
        #     There is a problem with eventNumber because it is float32, 
        #     and hits precision error. SO NO SORTING when using tfrecords
        # =============================================================================
        
            event_info = [np.c_[x['event_info'].numpy(),y.numpy()] for x, y in data]
            event_info = np.vstack(event_info)
            
    
            # Checking where eventNumber is 
            if ('Zee' in set_name) or ('Hyy' in set_name):
                if all([float(i).is_integer() for i in event_info[:3,1]]):
                    columns = ['p_e', 'eventNumber', 'p_eta', 'p_phi', 'p_eAccCluster', 'type','p_truth_e']
                elif all([float(i).is_integer() for i in event_info[:3,3]]):
                    columns = ['p_e', 'p_eta', 'p_phi', 'eventNumber','p_eAccCluster','type', 'p_truth_e']
            elif 'Zmumugam' in set_name:
                if isMC:
                    columns = ['p_e', 'p_eta', 'p_phi', 'eventNumber','p_eAccCluster',
                               'p_e_muon', 'p_eta_muon', 'p_phi_muon', 'eventNumber_muon','p_eAccCluster_muon',
                               'p_e_muon', 'p_eta_muon', 'p_phi_muon', 'eventNumber_muon', 'p_eAccCluster_muon',
                               'type', 'p_truth_e']                    
                else:
                    columns = ['p_e_muon', 'p_eta', 'p_phi', 'eventNumber', 'p_charge',
                               'p_e_muon', 'p_eta', 'p_phi', 'eventNumber','p_charge',
                               'p_e_photon', 'p_eta', 'p_phi', 'eventNumber','event_mass','p_charge', 'p_truth_e']


            data_array = pd.DataFrame(data = event_info, columns=columns)
            if isMC:
                print(np.abs(data_array['p_e']/data_array['p_truth_e']-1))
            norm = '/'.join(data_path.split('/')[:-2])+'norm/*averageInteractionsPerCrossing*'
            scaler = joblib.load(glob(norm)[0])
            averageInteractionsPerCrossing = np.hstack([x['scalars'].numpy()[:,0] for x, y in data_pileup])
            averageInteractionsPerCrossing.shape = (len(averageInteractionsPerCrossing), 1)
            try:
                data_array['averageInteractionsPerCrossing'] = scaler.inverse_transform(averageInteractionsPerCrossing)
            except ValueError:
                pass
            # print(averageInteractionsPerCrossing)
        else: # for h5 files
            #### FIT PEAK ####
            # if True:
            if isMC:
                data_params = ['p_LHMedium', 'p_LHTight', 'p_truth_eta', 'p_truth_phi',
                               'p_truth_pt', 'eventNumber','p_truthOrigin', 'p_truthType', 'p_truth_pdgId',
                               'p_LHLoose', 'p_eta', 'p_phi', 'p_et_calo', 'p_truth_e','p_e']
    
            else:
                data_params = ['eventNumber',
                               'p_eta', 'p_phi', 'p_e', 
                               #'p_MuonLHLoose','p_ElectronLHLoose', 'p_PhotonLHLoose',
                               'p_LHLoose',
                               #'p_ptvarcone20','p_ptvarcone30','p_ptvarcone40', 'p_topoetcone20', 'p_topoetcone30', 'p_topoetcone40',
                               # 'p_charge'
                               ]
    
            data = pd.DataFrame()
            with h5py.File(data_path, "r") as df:
                for i in data_params:
                    values = pd.DataFrame(data = np.array([df[set_name][i][:n_test]]).T, columns=[i])
                    data = pd.concat([data, values], axis=1)
            data = data.reset_index()
            # data = data.groupby("eventNumber").filter(lambda x: len(x) == 2)
        if len(last_info)==0:
            return data_array
        else:
            return pd.concat([last_info, data_array])
    
    def load_data(paths_data, n_test, data_params, set_name=set_name): # for h5 files
        # del data_params['track_names'] ### removing tracks variabels ##################
        data_params = {k: v for k, v in data_params.items() if v is not None}
        # all_data = []
        def concurrent_load_data(args):
            path_data = args
            global n_test
            if  (n_test == -1) and (data_type=='h5'):
                df = h5py.File(path_data,'r')
                n_test = len(df[set_name]['eventNumber'])
                df.close()
            n_points = {set_name:range(n_test)}
            data = dpcal.utils.load_atlas_data(path = path_data, n_points=n_points, verbose=verbose, **data_params)
            data = data[set_name]
            # data_params['track_names'] = None
            dpcal.tfrecord_data_creation.standardization(data=data['scalars'], data_params={'scalar_names': data_params['scalar_names'], 'track_names': None}, sample_name=set_name,
                                                dirs = path_norm+'norm/',
                                                tag=name, save=False)
            if 'track_names' in data_params.keys():
                dpcal.tfrecord_data_creation.standardization(data=data['tracks'], data_params={'scalar_names': None, 'track_names': data_params['track_names']}, sample_name=set_name,
                                                          dirs = path_norm+'norm/', tag=tag, save=False)
                track = data['tracks']
                track.shape = (track.shape[0], track.shape[1], track.shape[2], 1)
                data['tracks'] = track
            data['em_barrel'] = data['images']['em_barrel']
            del data['images']
            data = {k: v for k, v in data.items() if len(v) != 0}
    
            addisional_info = load_addisional_info(data=data, data_type='h5', data_path = path_data)
            return data, addisional_info
        results=[]
        addisional_info = pd.DataFrame()
        if True:
            print('Running concurrent')
            with concurrent.futures.ThreadPoolExecutor() as executor:
                result = executor.map(concurrent_load_data, paths_data)
        else:
            result = []
            for path in tqdm(paths_data, total = len(paths_data)):
                result.append(concurrent_load_data(path))
        for i,j in tqdm(result, total = len(paths_data)):
            results.append(i)
            addisional_info = pd.concat([addisional_info,j])
        data = {}
        for k in results[0].keys():
            data[k] = np.concatenate(list(d[k] for d in results))
        if True: # Tror måske bare det er fint #'event' in [i.name.split('_')[0] for i in model.inputs]:
            data['event_info'] = np.zeros((len(data['scalars']), 5))
        data = tf.data.Dataset.from_tensor_slices(data).batch(1024)
        return data, addisional_info
    
    
    #%%
    if data_type == 'h5':
        print('Importing data')
        data, addisional_info = load_data(paths_data=data_files, n_test=n_test, data_params = data_param)
        # print([i.name.split('_')[0] for i in model.inputs])
    
        print('Data loaded!')
    else:
        addisional_info = pd.DataFrame()
    #%%
    nr=0
    preds = pd.DataFrame()
    print('files: ', data_files)
    for models, weights, data_param in zip(model_paths, weights_paths, all_data_params):
        print(models)
        pred=[]
        model = load_model(model_path = models, weights_path = weights, verbose=verbose)

        if ((data_type != 'h5') & (nr == 0)) | (not nr%(len(sub_model_folders)*nr_models)):
            pileup = data_param.copy()
            pileup['scalar_names'] = ['averageInteractionsPerCrossing']
            data_pileup = dpcal.tfrecord_load_data.load_dataset(data_files[:nr_files], tag,
                                                         data_params = pileup,
                                                         load_single_file=True, shuffle=False,
                                                         merge=True,
                                                          ext_energy_range = (-hyperparameters['ext_energy_range']
                                                                              if ((hyperparameters['ext_energy_range'] != None) and isMC) else None),
                                                         additional_info=True, multiple_with=True,
                                                         batch_size = 256,
                                                         cut_ratio=hyperparameters['cut_ratio'] if isMC else None,
                                                         time_lr=data_param['gate_img_prefix'] != None)
            data = dpcal.tfrecord_load_data.load_dataset(data_files[:nr_files], tag,
                                                         data_params = data_param,
                                                         load_single_file=True, shuffle=False,
                                                         merge=True, 
                                                         ext_energy_range = (-hyperparameters['ext_energy_range']
                                                                              if ((hyperparameters['ext_energy_range'] != None) and isMC) else None),
                                                         additional_info=True, multiple_with=True,
                                                         batch_size = 256,
                                                         cut_ratio=hyperparameters['cut_ratio'] if isMC else None,
                                                         time_lr=data_param['gate_img_prefix'] != None)
            if nr == 0:
                addisional_info = load_addisional_info(data, data_type, data_path = data_files[0], last_info = addisional_info,
                                                       data_pileup=data_pileup)
            print('Data loaded!')
    
        pred.extend(model.predict(data))
    
        # Predict
        pred = np.array(pred)
        # print(pred)
        if len(pred[0]) == 2:
            pred = pd.DataFrame(data=pred, columns = ['CNN_ER_'+str(nr), f'sigma_{nr}'])
        elif len(pred[0]) == 6:
            pred = pd.DataFrame(data=pred, columns = ['CNN_ER_'+str(nr), 'p_e', 'eventNumber', 'p_eta', 'p_phi', 'E_Acc'])
        elif len(pred[0]) >= 6:
            pred = pd.DataFrame(data=pred, columns = ['CNN_ER_'+str(nr), 'p_e', 'p_eta', 'p_phi', 'eventNumber','p_MuonLHLoose_-1_PhotonLHLoose_1',
                                                      'p_e_muon', 'p_eta', 'p_phi', 'eventNumber','p_MuonLHLoose_-1_PhotonLHLoose_1',
                                                      'p_e_muon', 'p_eta', 'p_phi', 'eventNumber', 'p_MuonLHLoose_-1_PhotonLHLoose_1'])
        else:
            pred = pd.DataFrame(data=pred, columns = ['CNN_ER'+
                                                      models.split('epochs')[-1]
                                                      .replace('/saved_models/', '_')
                                                      .split('/')[0]+'_'+str(nr)])
            pred['parameters'] = model.count_params()
        preds = pd.concat([preds, pred], axis=1)
        nr+=1
    
    # print(addisional_info)
    # if (name == 'Zmumugam') & (not isMC):
    #     data = preds
    # else:
    data = pd.concat([addisional_info.reset_index(),preds.reset_index()],axis=1)
    data = data.drop(['index'], axis=1)
    if not 'Zmumugam' in name:
        data = data.loc[:,~data.columns.duplicated()]

    data = data[data[preds.columns[0]] > 0]
    # data = data.astype({'eventNumber':'int64'})
    # data = data.sort_values(by=['eventNumber'])
    #%%
    if save:
        if (name == 'Zmumugam') & (isMC):
            # files = glob('../root2hdf5/output/root2hdf5/Zmumugam_mc_all_shuffled/*.h5')
            # columns = ['eventNumber', 'p_eta', 'p_phi', 'p_truth_e', 'p_e', 'p_truth_pdgId']
            # df_all = pd.DataFrame()
            # for i in files[:]:
            #     df_file = h5py.File(i)
            #     for j in ['train', 'test', 'val']:
            #         df = pd.DataFrame(data = np.array([df_file[j][i] for i in columns]).T, columns=columns)
            #         if not isMC:
            #             print('LHLoose not applied yet')
            #             sys.exit()
            #         else:
            #             df = df[df['p_truth_pdgId'] != 22]
            #         df = df.groupby("eventNumber").filter(lambda x: len(x) == 2)
            #         df_all = pd.concat([df_all, df], axis=0)
    
    
            # data = pd.concat([df_all, data])
            # del df_all
            # del df
            # # data = data.sort_values(by = 'eventNumber')
            # number_of_particles = 2 if (name == 'Zee') or (name == 'Hyy') else 3 if name == 'Zmumugam' else 'unknown'
            # data = data.groupby("eventNumber").filter(lambda x: len(x) == number_of_particles)
            
            print(data)
    
        model = '_'.join(['sigma' if 'sigma' in i else 'mean' for i in folder])
        performance = '_'.join([i.split('-')[-1].split('.json')[0] for i in model_paths])
        # file_name = f'{name}_events_{len(data)}_pred_{"mc" if isMC else "data"}_models_{model}_{performance}.pkl'
        
        # file name
        file_name = '_'.join(folder[:1])
        file_name = file_name.replace('epochs', f'e_pred_{len(data)}').replace('mc', set_name)+'_nr_model_'+str(len(folder))+'.pkl'
        
        # file path
        file_name_of_predictions = f'./logs/{gpu_dir}/{folder[0]}/performance/'
        if not os.path.isdir(file_name_of_predictions):
            os.mkdir(file_name_of_predictions)
        data.to_pickle(file_name_of_predictions+f'{file_name}')
        user_answer = []
        if isMC:
            run_DeepCalo_MC_for_mc_analysis(file=file_name_of_predictions+f'{file_name}',
                                            metric=['energy', 'mass'], analysis=[[''], ['']], n=None,
                                            number_of_methods=1, path = file_name_of_predictions,
                                            save=True)
        else:
            run_DeepCalo_eval_data(file=file_name_of_predictions+f'{file_name}',
                                   metric=['mass'], analysis=[['']], n=None,
                                   number_of_methods=None, path = file_name_of_predictions,
                                   save=True)


def get_params():
    """
    Returns a dictionary containing parameters to be passed to the model
    container.

    Please see the README for documentation.
    """
    BWxCB_fit = False
    step = 20*4000#100_000 # Zee_mc: 3000, Zmumugam_mc: 800
    if BWxCB_fit:
        batch = int(512*11) # 11
    else:
        batch = 2048
    params = {
          # Submodels
          'track_net'                  : {'use': True,  ######################
                                          'output_size': 15,
                                          'connect_to': ['top']
                                          },

          'top'                        : {'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'units':[256,256,1],
                                          'final_activation':'relu', ##### relu regression - sigmoid classification
                                          'probability_layer' : False,
                                          'BWxCB_fit': BWxCB_fit}, ################################
          'cnn'                        : {'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'block_depths':[1,2,2,2,2],
                                          'n_init_filters':16,
                                          'downsampling':'maxpool',
                                          'min_size_for_downsampling':6},
          'scalar_net'                 : {'use': True, #################
                                          'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'units':[32],
                                          'connect_to':['top'], #FiLM_gen 'top']
                                          },
          'gate_net'                   : {
                                          'initialization':'orthogonal',
                                          'activation':None,
                                          'normalization':None,
                                          'layer_reg':{},
                                          'dropout':None,
                                          'units':[],
                                          'use_res':False,
                                          'final_activation':None,
                                          'final_activation_init':[1.0],
                                          'connect_to':'concatenate' # concatenate  multiply Before called merge_method
                                          },
          'FiLM_gen'                   : {'use':False, #######################
                                          'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'units':[512,1024]}, # 512,1024
          }

    return params

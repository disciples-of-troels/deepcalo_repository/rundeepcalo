def get_params():
    """
    Returns a dictionary containing parameters to be passed to the model
    container.

    Please see the README for documentation.
    """
    BWxCB_fit = False
    if BWxCB_fit:
        batch = int(512*11) # 11
    else:
        batch = 2048
    number_of_points = 14624*512
    # =============================================================================
    #     step size 
    # =============================================================================
    # Zee ensemble: int(1000*2048+14624*512*0.6)
    # Zee mc: 14624*512 data:1100*2048
    #Zmumugam 1581056 - new Zee: 5861*2048  Hyy: 1614*2048
    # electron gun 2628*2048
    
    step = 20*(number_of_points/batch)
    params = {
          # Training
          'epochs'                     : 1000, #########################################
          'batch_size'                 : batch,
          'cut_ratio'                  : 0.6, #0.8, ########################################
          'ext_energy_range'           : None, ########################################
          'loss'                       : 'logcosh', #'logcosh', cls : binary_crossenstropy
          'metrics'                    : [],#['mae', 'mse'], #cls: accuracy
          'optimizer'                  : 'Nadam',
          'lr_finder'                  : {'use':False,   #########################
                                          'scan_range':[5e-6, 1e-1],# 8e-8, 1e0
                                          'epochs':2,
                                          'Number_of_files':75,
                                          'prompt_for_input':False,
                                          'scale': 'linear',
                                          'number_of_runs': 1},
          'lr_schedule'                : {'name':'OneCycle', # CLR OneCycle
                                          'range': [0.0001, 0.001], #[0.0001, 0.01], #[5e-4, 7e-2], # [3e-5, 1e-3] 0.0005
                                          'step_size_factor': step,
                                          # 'kwargs': {
                                          #       'mode': 'exp_range',
                                          #       'gamma': step/(step+1),
                                          #      }
                                              },
          'auto_lr'                    : False,
          'profiler'                   : False,

          # Misc.
          'data_generator'            : {
                                          'use':True, # really not using a datagen any more
                                          'filesize': [-1,-1], #[[0.6,0.6], [1,1]],#[-1, -1], # ########################## [-1, 10] #Zee [[0.2,0.2], [1,1]]
                                          'n_workers':10,
                                          'max_queue_size':10
                                           },

          'use_earlystopping'          : {'use': True,
                                          'patience': 30,
                                          'delta': 0.005},
          'restore_best_weights'       : True,

          'pretrained_model'           : {'use':False,
                                          'weights_path':'./logs/sigma_model/Zee_mc_2021-07-23T20:34:01.894604_500_epochs_sigma_model_stabil_G_avg_pooling/saved_models/0/weights.0037-1.6622.hdf5',
                                          'params_path':'./logs/sigma_model/Zee_mc_2021-07-23T20:34:01.894604_500_epochs_sigma_model_stabil_G_avg_pooling/hyperparams_0.pkl',
                                          'layers_to_load':['top', 'cnn', 'FiLM_generator', 'scalar_net', 'tracks'],
                                          'freeze_loaded_layers':False},


          'upsampling'                 : {'use':True,
                                          'wanted_size':(56,55)},

          # Submodels
          'track_net'                  : {'use': True,  ######################
                                          'output_size': 16,
                                          'nr_neurons': [128,64,32],
                                          'conv1d': 5,
                                          'connect_to': ['top']
                                          },

          'top'                        : {'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'units':[512,512,1],
                                          'final_activation':'relu', ##### relu regression - sigmoid classification
                                          'probability_layer' : True, ######
                                          'BWxCB_fit': BWxCB_fit}, ################################
          'cnn'                        : {'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'block_depths':[1,4,4,4],#[1,2,2,2,2,2], # [1,2,2,2,2], [1,4,4,4]
                                          'n_init_filters':16,
                                          'downsampling':'maxpool', # avgpool maxpool
                                          'globalpooling':'maxpool', # maxpool/avgpool
                                          'min_size_for_downsampling':2},
          'scalar_net'                 : {'use': True,
                                          'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'units':[256],
                                          'connect_to':['FiLM_gen'], #FiLM_gen 'top']
                                          },
          'gate_net'                   : {
                                          'initialization':'orthogonal',
                                          'activation':None,
                                          'normalization':None,
                                          'layer_reg':{},
                                          'dropout':None,
                                          'units':[],
                                          'use_res':False,
                                          'final_activation':None,
                                          'final_activation_init':[1.0],
                                          'connect_to':'concatenate' # concatenate  multiply Before called merge_method
                                          },
          'FiLM_gen'                   : {'use':True,
                                          'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'units':[512,1024]}, # 512,1024
          }

    return params

def get_params():
    """
    Returns a dictionary containing parameters to be passed to the loading
    function.
    """

    params = {'target_name'            : 'p_truth_e',#'ATLAS_91_energy', # p_truth_e ratio mask_atlas_energy
              'img_names'              : ['em_barrel'], # NOTE: Should actually be the added barrel and endcap layers!
              'gate_img_prefix'        : [
                                            'time_em_barrel',
                                           # 'gain_em_barrel'
                                          ], #['time_em_barrel_Lr0', 'time_em_barrel_Lr1', 'time_em_barrel_Lr2', 'time_em_barrel_Lr3'], 
               'scalar_names'           : [ # Set to None if you only want to use the images
                                            'p_eAccCluster',
                                            'p_cellIndexCluster',
                                            'p_f0Cluster',
                                            'p_R12',
                                            'p_pt_track',
                                            'p_nTracks',
                                            'p_eta',
                                            'p_deltaPhiRescaled2',
                                            'p_etaModCalo',
                                            'p_deltaEta2',
                                            'NvtxReco',
                                            # 'averageInteractionsPerCrossing',
                                            'p_poscs2',
                                            'p_dPhiTH3',
                                            'p_fTG3',
                                            'tile_gap_Lr1',
                                            # 'p_qOverP'# not in images
                                            'type',
                                           ],
              'track_names'            : [ # Set to None if you don't want to use tracks.
                                           # It is recommended to also add p/q and d0/sigma_d0
                                            'tracks_dR', 'tracks_pt',
                                            # 'tracks_sigmad0',
                                            'tracks_theta', 'tracks_trthits',
                                            'tracks_vertex', 'tracks_d0',
                                            'tracks_z0',
                                            # 'tracks_charge',
                                            'tracks_eta', 'tracks_phi',
                                            'tracks_pixhits', 'tracks_scthits'
                                           ],
              'max_tracks'             : None,
              'multiply_output_name'   : 'p_eAccCluster',#'p_eAccCluster', #'p_eAccCluster',
              'sample_weight_name'     : None,
              'additional_info'        : True,
              }

    return params

import os
os.environ['TF_GPU_THREAD_MODE'] = 'gpu_private'
os.environ['TF_GPU_THREAD_COUNT'] = '2'
import copy
import time
import numpy as np
import argparse
import pickle
import h5py
import gc
import sys
import copy
# tf.enable_eager_execution()
from os import listdir
from os.path import isfile, join
from numba import cuda 
import itertools
from glob import glob
if True:
    folders = [
                # 'Zee_mc_2021-09-01T15:55:42.210181_1000_epochs_Zee_histogram_eq',
                'Zmumugam_mc_2021-08-30T21:16:54.185802_1000_epochs_Zmumugam_full_model'
                # 'Zee_data_2021-09-02T09:45:13.642870_1000_epochs_Zee_train_data_full_model'
                # 'Zee_data_2021-09-03T07:53:58.684978_1000_epochs_Zee_ensemble_full_model'
              ]
    # folders = glob('./logs/testing_architecture1/*') 
    # folders = [i.split('/')[-1] for i in folders]
    all_displays = {'mass':[], 'energy':[]}
    n=None
    new_pred = True
    for folder in folders:
        for gpu_dir in ['0', '0_1', '1', 'test_time_lr',
                        'performance_measures', 'benchmarks',
                        'testing_architecture', 'testing_architecture1',
                        'sigma_model', 'models_in_use']:
            try:
                hyperparameters = glob(f'./logs/{gpu_dir}/'+folder+'/hyper*.pkl')[0]
                with open(hyperparameters, 'rb') as handle:
                    print('GPU dir:', gpu_dir)
                    hyperparameters = pickle.load(handle)
                break
            except IndexError:
                print('No Hyperparameter.pkl file!')
                gpu_dir = None
        if gpu_dir == None:
            sys.exit()
        with open(glob(f'./logs/{gpu_dir}/'+folder+'/data*.pkl')[0], 'rb') as f:
            data_params = pickle.load(f)
        if new_pred:
            sys.path.append('..')
            import deepcalo as dpcal
            from tensorboard.plugins.hparams import api as hp
            from keras.backend import clear_session
            from tensorflow.python.framework.ops import disable_eager_execution
            for data_name in [
                                # data_params['sub_data_folder_name'].replace('data', 'mc').replace('train', 'test'),
                                data_params['sub_data_folder_name'].replace('mc', 'data').replace('train', 'test')
                              ]:
                gpu = data_params['flags']['gpu'].replace('_',',')
                gpu = 1
                str_to_run = (f"python load_model_advanced.py -g {gpu} --data_path {data_params['data_path']} "
                             f"--gpu_dir {gpu_dir} --sub_data_folder_name {data_name} ") # /{data_params['flags']['gpu']} 
                # for i in model_names:
                str_to_run+= f"--model_path {folder} "
                print(); print(str_to_run); print()
                device = cuda.get_current_device()
                device.reset()
                os.system(str_to_run) 
        else:
            sys.path.append('../invmfit')
            from mc_performance import *
            from data_performance import *
            for file in glob(f'./logs/{gpu_dir}/{folder}/performance/*.pkl'):
                user_answer = []
                if 'mc' in file.split('/')[-1]:
                    # pass
                    display = run_DeepCalo_MC_for_mc_analysis(file=file,
                                                    metric=['energy', 'mass'], analysis=[[''], ['']], n=n,
                                                    number_of_methods=None, path = '/'.join(file.split('/')[:-1])+'/',
                                                    save=True)
                    all_displays['energy'].append(copy.deepcopy(display))
                else:
                    pass
                    display = run_DeepCalo_eval_data(file=file,
                                            metric=['mass'], analysis=[['']], n=n,
                                            number_of_methods=None, path = '/'.join(file.split('/')[:-1])+'/',
                                            save=False)
                    all_displays['mass'].append(copy.deepcopy(display))
                
    #%%
    if False:
        data = {}
        for nr, folder in enumerate(folders):
            name = folder.split('epochs_')[-1]
            data[name] = {}
            display_mass = all_displays['mass'][nr]
            display_energy = all_displays['energy'][nr]
            df_mass = pd.DataFrame()
            for i_mass in [display_mass, display_energy[1]]:
                mask_variables = len(i_mass.targets_values['p_e_mass_']) == i_mass.variables.n
                fit_values = i_mass.variables[mask_variables]
                df_mass = pd.concat([df_mass , fit_values])
            data[name]['mass'] = df_mass
            data[name]['energy'] = display_energy[0].reIQR
            
        a_file = open("testing_architecture1.pkl", "wb")
        pickle.dump(data, a_file)
        a_file.close()
else:
    import pandas as pd 
    output = pd.read_pickle('testing_architecture1.pkl')
    #%%
    all_data=pd.DataFrame()
    for key in output.keys():
    
        mass = output[key]['mass']
        energy = output[key]['energy']
        # data CB
        for i in mass.n.drop_duplicates():
            mask_type = (mass.n == i)
            datatype = 'mc' if any(['truth' in i for i in mass[mask_type].name]) else 'data'
            cnn_col = []
            for col in mass[mask_type].name:
                if 'CNN' in col:    
                    cnn_col.append(col)
            mask_col = np.in1d(mass[mask_type].name, cnn_col)
            if datatype == 'mc':
                mc_value = np.mean(mass[mask_type][mask_col]['sigmaCB'])
            else:
                data_value = np.mean(mass[mask_type][mask_col]['sigmaCB'])
        
        value = np.array([energy[list(energy.keys())[0]]['rIQR: 75'], energy[list(energy.keys())[0]]['rIQR: 95'],
                 mc_value, data_value])
        value.shape = (1,4)
        value = pd.DataFrame(value, columns = ['reIQR75', 'reIQR95', 'sigmaCB mc', 'sigmaCB data'],
                             index = [key])
        all_data = pd.concat([all_data, value]).sort_index()
        all_data.index = [i.replace('_', ' ').replace('FiLM', 'FiLM:').replace('top', '- top:') for i in all_data.index]
    
    
    
    
    
    
    
    
    
    
    
    

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 25 16:30:40 2021

@author: malte
"""


if __name__ == '__main__':
    from glob import glob
    import os
    import numpy as np
    from numba import cuda 
    run_type = ['electron']
    for name in run_type:
        model_variants = glob(f'params/{name}_models/*')
        gpu = 1
        mask = np.array([('py' in i) or ('__' in i) for i in model_variants])
        model_variants = np.array(model_variants)[~mask]
        for variant in model_variants:
            print('# '*10, variant)
            str_to_run = (f"python train_model.py -g {gpu} -n {variant.split('/')[-1]}"
                          f" --param_path {variant} --exp_dir ./logs/testing_architecture/")
            print(str_to_run)
            device = cuda.get_current_device()
            device.reset()
            os.system(str_to_run) 
import os
os.environ['TF_GPU_THREAD_MODE'] = 'gpu_private'
os.environ['TF_GPU_THREAD_COUNT'] = '2'
import copy
import time
import numpy as np
import argparse
import pickle
import h5py
import gc
import sys
sys.path.append('..')
import deepcalo as dpcal
import tensorflow as tf
from glob import glob
# tf.enable_eager_execution()
from os import listdir
from os.path import isfile, join
from numba import cuda
import itertools
from tensorboard.plugins.hparams import api as hp
from keras.backend import clear_session
from tensorflow.python.framework.ops import disable_eager_execution

#%%
# changing path so the script can be runned locally
if ((os.getcwd() == '/home/malte/hep/work/rundeepcalo') |
    (os.getcwd() == '/home/malte-gtx970/hep/gpulab/work/Master/run/train_model_malteal')):
    print('local run')

    exp_dir = './'
    gpu_ids = '0'
    _n_train = 100
    _n_val = 50
    _n_test = 10
    # path = '../../../../../gpulab-data/Master/tfrecords/Hyy_mc_with_p_e/'
    # path_local = './data/Hyy_mc_with_p_e/'
    path = '../tfrecords_data/Hyy_mc_with_p_e_test/'

    # path = '../tfrecords_data/Zee_mc_with_p_e_test_sorted/'
    # path = '/data/Zee'
    path = '../tfrecords_data/Zmumugam_data/'#old_data/'
    # path = ['../tfrecords_data/Zee_mc_w_time_large_reweighted/', '../tfrecords_data/Zee_mc_w_time_large_reweighted/']
    # path = ['../tfrecords_data/Zee_mc_w_time_large_reweighted/', '../tfrecords_data/Zee_mc_w_time_large_reweighted/'] # Zee_data_w_time_peak
    path = '../tfrecords_data/Zee_w_new_track_var/'
    # path = '../tfrecords_data/Zee_mc_w_time_large_reweighted/'#Zee_mc/'#old_data/' Zmumugam_mc_shuffle Zmumugam_data
    cache =True
    shuffle=False
else:
    # path = ['../tfrecords_data/Zee_mc_w_time_large_reweighted/', '../tfrecords_data/Zee_mc_w_time_large_reweighted/'] # Zee_data_w_time_peak
    # path = '../tfrecords_data/Zee_new_standardization/'
    path = '../tfrecords_data/Zee_w_new_track_var/' # Zee_w_new_track_var Zee_histogram_equalization
    # path = '../tfrecords_data/Zee_normalization_from_data/'#Zee_mc/'#old_data/' Zmumugam_mc_shuffle Zmumugam_data
    cache = False
    shuffle = 2048
    tag = 'Zee' #('Zmumugam' if 'Zmumugam' in path
            # else 'Zee' if 'Zee' in path
            # else 'Hyy' if 'Hyy' in path
            # else print('Unknown long tag'))

run_same_model_x_times = 2 # 0 disable
classification=False
nr_of_cores = 32 # none means -1

sample_type= 'mc'#('mc' if 'mc' in path
              # else 'data' if 'data' in path
              # else 'mc')

electron_tag = True if 'Zee' in tag else False

particle = 'electron' if tag else 'photon'

#%%
# ==============================================================================
# Argument parsing
# ==============================================================================
parser = argparse.ArgumentParser()
parser.add_argument('-g','--gpu', help='Which GPU(s) to use, e.g. "0" or "1,3".', default='0', type=str)
parser.add_argument('--data_path', help='Path to data. Can be relative.',
                    default=path)
parser.add_argument('--exp_dir', help='Directory of experiment, e.g. "my_exp/".', default='./', type=str)
parser.add_argument('-n','--name', help='Addisional name to output dir', default='', type=str)
parser.add_argument('--n_val', help='How many data points to load from the validation set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
parser.add_argument('--n_test', help='How many data points to load from the test set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
parser.add_argument('--rm_bad_reco', help='Removes all points that are badly reconstructed by the current energy calibration.', default=False, type=dpcal.utils.str2bool)
parser.add_argument('--zee_only', help='Uses only Z->ee data.', default=False, type=dpcal.utils.str2bool)
parser.add_argument('--lh_cut_name', help='Which, if any, likehood cut to apply. Should be the name of the variable that should be True for that point to not be masked out. E.g., for electrons, use "p_LHLoose", while for photons, use "p_photonIsLooseEM" (or "Medium" or "Tight"). None (default) will not apply any likehood cut.', default=None, type=str)
parser.add_argument('--rm_conv', help='Mask out converted photons.', default=None, type=dpcal.utils.str2bool)
parser.add_argument('--rm_unconv', help='Mask out unconverted photons.', default=None, type=dpcal.utils.str2bool)
parser.add_argument('--save_figs', help='Save figures.', default=True, type=dpcal.utils.str2bool)
parser.add_argument('--param_path', help='Path to templete conf', default=None, type=str) # param_conf
parser.add_argument('-v','--verbose', help='Verbose output, 0, 1 or 2, where 2 is less verbose than 1.', default=1, type=int)
args = parser.parse_args()


#%%
gpu_ids = args.gpu
param_path = args.param_path
if param_path is not None:
    files = glob(param_path+'/*')
    mask = np.array([('__' in i) for i in files])
    files = np.array(files)[~mask]
    mask_type = np.array(['param_conf' in i for i in files])
    data_conf_path = files[~mask_type][0]
    param_conf_path = files[mask_type][0]
    print(param_conf_path, data_conf_path)
data_path = args.data_path
exp_dir = args.exp_dir
if not os.getcwd() == '/home/malte/hep/gpulab/work/Master/run/train_model_malteal':
    _n_train = int(-1)
    _n_val = int(-1)
    _n_test = int(-1)
save_figs = args.save_figs
rm_bad_reco = args.rm_bad_reco
zee_only = args.zee_only
lh_cut_name = args.lh_cut_name
rm_conv = args.rm_conv
rm_unconv = args.rm_unconv
verbose = args.verbose
name_addisional_info = args.name
apply_mask = rm_bad_reco or zee_only or lh_cut_name or rm_conv or rm_unconv

tf.config.set_soft_device_placement(True)
os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_ids
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
# =============================================================================
# 
# =============================================================================

if 'ensemble' in name_addisional_info:
    ensemble = {
                tag: ['mc', 'data'],
                # 'Zmumugam': ['mc']
                }
else:
    ensemble = None


if tf.test.gpu_device_name():
    gpus = tf.config.experimental.list_physical_devices('GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)
    print('Default GPU Device: {}'.format(tf.test.gpu_device_name()))

else:

   print("Please install GPU version of TF")
# ==============================================================================
# Get hyperparameters
# ==============================================================================
# Import parameter configurations that should be different from the default as a module
# package should be the folder of the file
from importlib import import_module
param_conf = import_module('..param_conf', 'params.subpkg')
params = param_conf.get_params()

if param_path is not None:
    param_conf2 = param_conf_path.replace('params', '.').replace('/', '.').replace('.py', '')
    param_conf2 = import_module(param_conf2,  'params.subpkg')
    param_conf2 = param_conf2.get_params()
    for key in param_conf2.keys():
        params[key] = param_conf2[key]
#%%
# Use the chosen parameters where given, and use the default parameters otherwise
params = dpcal.utils.merge_dicts(dpcal.utils.get_default_params(), params, in_depth=True)

# Set learning rate based on optimizer and batch size
if params['auto_lr']:
    params = dpcal.utils.set_auto_lr(params)

# Integrate the hyperparameters given from the command line
params['n_gpus'] = len(gpu_ids.replace(',',''))
#%%
# ==============================================================================
# Logging directories
# ==============================================================================
# Make directories for saving figures and models
# dirs is a dictionary of paths
dirs = dpcal.utils.create_directories(exp_dir, tag+'_'+sample_type,
                                      params['epochs'], log_prefix=gpu_ids,
                                      name_addisional_info=name_addisional_info)


if electron_tag:
    data_conf = import_module('..electrons_data_conf',  'params.subpkg')
else:
    data_conf = import_module('..photons_data_conf',  'params.subpkg')
data_params = data_conf.get_params()

if param_path is not None:
    data_conf2 = data_conf_path.replace('params', '.').replace('/', '.').replace('.py', '')
    data_conf2 = import_module(data_conf2,  'params.subpkg')
    data_conf2 = data_conf2.get_params()
    for key in data_conf2.keys():
        data_params[key] = data_conf2[key]
#%%
if params['data_generator']['use']:
    if data_path is not None:
        params['data_generator']['path'] = data_path
        params['data_generator']['load_kwargs']
    params['data_generator']['load_kwargs']['path'] = data_path
    params['data_generator']['load_kwargs'].update(data_params)
    pass

def define_and_train_model(*,data, params, dirs, save_figs, data_path,
                           verbose=1, hparams=None, datatype=None, session_num="",
                           testing=True, train_lr_finder=None):
    start_time = time.time()
    # ==============================================================================
    # Create and train model
    # ==============================================================================
    # Instantiate model container (with self.model in it)
    if (session_num != "") & (hparams==None):
        dirs['saved_models'] += session_num+'/'
        dpcal.tfrecord_data_creation.create_dir(dirs = dirs['saved_models'], quit_script=True)
    if (session_num != "") & (hparams!=None):
        name = [i.name.split('-')[0]+'_'+str(j) for i, j in zip(hparams, hparams.values())]
        dirs['saved_models'] +='-'.join(name)+f'_{session_num}/'
        dpcal.tfrecord_data_creation.create_dir(dirs = dirs['saved_models'], quit_script=True)

        #Train model

    # Save data parameters (with some additional information)
    data_params['data_path'] = data_path
    data_params['flags'] = args.__dict__
    data_params['sub_data_folder_name'] = train_files[0].split('/')[-2]
    data_params['model_path']=dirs['log'].split('/')[-2]
    if session_num == '':
        dpcal.utils.save_dict(data_params, dirs['log'] + f'dataparams.txt', save_pkl=True)
    else:
        dpcal.utils.save_dict(data_params, dirs['log'] + f'dataparams_{session_num}.txt', save_pkl=True)

    #Train model
    # sys.exit()
    mc = dpcal.ModelContainer(data=data,
                              params=params,
                              dirs=dirs,
                              save_figs=save_figs,
                              verbose=verbose,
                              data_path=data_path,
                              hparams=hparams,
                              datatype=datatype,
                              nr_of_cores=nr_of_cores,
                              train_lr_finder=train_lr_finder,
                              data_params=data_params,
                              session_num=session_num)

    # Save hyperparameters (with some additional information)
    params_for_saving = copy.deepcopy(mc.params)
    params_for_saving['n_params'] = mc.model.count_params()

    if session_num == '':
        dpcal.utils.save_dict(params_for_saving, dirs['log'] + f'hyperparams.txt', save_pkl=True)
    else:
        dpcal.utils.save_dict(params_for_saving, dirs['log'] + f'hyperparams_{session_num}.txt', save_pkl=True)

    results = None
    if testing:

            # del mc
        if False:
            os.system(f"python load_model_advanced.py -g {data_params['flags']['gpu'].replace('_',',')} --data_path {data_params['data_path']} "
                      f"--gpu_dir {data_params['flags']['gpu']} --sub_data_folder_name {data_params['sub_data_folder_name']} "
                      f"--model_path {data_params['model_path']}")
            # Evaluate (predicting and evaluating on test or validation set)
    if hparams is not None:
        if not hasattr(mc,'evaluation_scores'):
            mc.evaluate(predict=False)
            results = mc.evaluation_scores

        # Print results
        if verbose:
            print('Evaluation scores:')
            print(mc.evaluation_scores)
        save_performance = open(dirs['log'] +str(mc.evaluation_scores)+f'_{session_num}.txt', 'wt')
        save_performance.write(str(mc.evaluation_scores))
        save_performance.close()
    print('%'*50)
    print(f'Time: {round(time.time()-start_time,2)} sec')
    print('%'*50)

    # Clear memory - maybe remove if it removes cache
    clear_session()
    gc.collect()
    del mc
    return results, data_params

def change_param_values(params, param_names, hparams):
    for i, value in zip(param_names, hparams.values()):
        if ':' in i:
            sub_element, name = i[0].split(':')
        else:
            sub_element, name = '', i[0]
        if ((name=='block_depths') or (name=='number_of_blocks') or
            (name=='number_of_units') or (name=='units')):
            continue
        if len(i) == 1:
            if sub_element != '':
                params[sub_element][name] = value
            else:
                params[name] = value
        elif len(i) == 2:
            params[i[0]][i[1]] = [value, value*2]
        else:
            print('param_names is to long - do not know what to do')
            sys.exit()
    keys = np.array(list(hparams.keys()))[np.array([i[0] in ['cnn:number_of_blocks', 'cnn:block_depths',
                                                             'FiLM:number_of_units', 'FiLM:units',
                                                             'top:number_of_units', 'top:units'] for i in param_names])]
    for i in keys:
        if 'block_depths' in i.name:
            block_depths = hparams[i]
        elif 'number_of_blocks' in i.name:
            number_of_blocks = hparams[i]
        elif 'number_of_units' in i.name:
            number_of_units = hparams[i]
        elif 'units' in i.name:
            units = hparams[i]
    if any(['FiLM' in i.name for i in keys]):
        params['FiLM_gen']['units'] = [units]*number_of_units
        # params['FiLM_gen']['units'] += [1]
    elif any(['top' in i.name for i in keys]):
        params['top']['units'] = [units]*number_of_units
        params['top']['units'] += [1]
    elif any(['cnn' in i.name for i in keys]):
        params['cnn']['block_depths'] = [1]+[block_depths]*number_of_blocks
    return params

def run(*, run_dir, hparams, data, params, dirs, save_figs, data_path,
        session_num, verbose=1, run_same_model_x_times):
  with tf.summary.create_file_writer(run_dir).as_default():
    hp.hparams(hparams)  # record the values used in this trial
    param_names = [i.name.split('-') for i in hparams.keys()]
    params = change_param_values(params.copy(), param_names, hparams)
    results = []
    for i in range(run_same_model_x_times):
        print('# '*10, run_same_model_x_times)
        result, _ = define_and_train_model(data=data, params=params, dirs=dirs.copy(),
                                          save_figs=save_figs, data_path=data_path,
                                          verbose=1, hparams=hparams, session_num=str(session_num)+str(i))
        # result = 1
        results.append(list(result.values()))
    print(results)
    results = np.mean(results,0)
    print(results[0])
    tf.summary.scalar(METRIC_ACCURACY, results[0], step =1)
    tf.summary.scalar('MAE', results[1], step =1)
    tf.summary.scalar('MSE', results[2], step =1)
  return None
#%%

if True:
    # =============================================================================
    # Loading data
    # =============================================================================
    if ensemble != None:
        if (params['data_generator']['filesize'][0][0] >1) & (params['data_generator']['filesize'][1][0] >1):
            print('Number of files has to be a ratio')
            sys.exit()
        train_files=[]
        val_files=[]
        # for i, ratio, sample_type in zip(path, params['data_generator']['filesize'], types):
        for tag_ensemble in ensemble.keys():
            for sample_type in ensemble[tag_ensemble]:
                train_path = f'{tag_ensemble}_train_{sample_type}'
                val_path = f'{tag_ensemble}_val_{sample_type}'
                if classification:
                    train_path += '_classification'
                    val_path += '_classification'
                # if params['data_generator']['filesize']d
                ratio = params['data_generator']['filesize'][1 if sample_type=='data' else 0]
                train_f = [path+train_path+'/'+f for f in os.listdir(path+train_path) if os.path.isfile(os.path.join(path+train_path, f))] # path_local
                val_f = [path+val_path+'/'+f for f in os.listdir(path+val_path) if os.path.isfile(os.path.join(path+val_path, f))]
                train_files.extend(train_f[:int(len(train_f)*ratio[0])])
                val_files.extend(val_f[:int(len(train_f)*ratio[1])])
    else:
        train_path = f'{tag}_train_{sample_type}' # w_gain _single
        val_path = f'{tag}_val_{sample_type}'
        if classification:
            train_path += '_classification'
            val_path += '_classification'
        train_files = [path+train_path+'/'+f for f in os.listdir(path+train_path) if os.path.isfile(os.path.join(path+train_path, f))] # path_local
        # train_files = np.array(train_files)[np.argsort([int(i.split('_')[-2])for i in train_files])]
        val_files = [path+val_path+'/'+f for f in os.listdir(path+val_path) if os.path.isfile(os.path.join(path+val_path, f))]
        # val_files = np.array(val_files)[np.argsort([int(i.sHitting the invariant massplit('_')[-2])for i in val_files])]
        filesize=[]
        for i, files in zip(params['data_generator']['filesize'], [train_files, val_files]):
            if i == -1:
                filesize.append(len(files))
            elif i< 1:
                filesize.append(int(i*len(files)))
            else:
                filesize.append(i)
        train_files= train_files[:filesize[0]]
        val_files= val_files[:filesize[1]]

    multiple_with = not (data_params['multiply_output_name'] == None)
    autotune = tf.data.experimental.AUTOTUNE if nr_of_cores == None else nr_of_cores
    use_time_lr = data_params['gate_img_prefix'] == 'time_em_barrel'
    np.random.shuffle(train_files)
    #%%
    print('#'*5, train_files, '#'*5)
    train = dpcal.tfrecord_load_data.load_dataset(filenames = train_files,
                                                      tag= tag, shuffle=shuffle, cache=False,
                                                      multiple_with=multiple_with, load_single_file=params['top']['BWxCB_fit'],
                                                      additional_info=True, batch_size = params['batch_size'],
                                                      cut_ratio=params['cut_ratio'],
                                                      time_lr=any(['time' in i for i in data_params['gate_img_prefix']]),
                                                      data_params=data_params,
                                                      autotune=autotune,ext_energy_range=params['ext_energy_range']).prefetch(buffer_size=autotune)#.repeat(10000)

    val = dpcal.tfrecord_load_data.load_dataset(filenames = val_files,
                                                    tag= tag, shuffle=0, cache=cache,#cache,
                                                    multiple_with=multiple_with, load_single_file=params['top']['BWxCB_fit'],
                                                    additional_info=True, batch_size = params['batch_size'],
                                                    cut_ratio=params['cut_ratio'],
                                                    time_lr=any(['time' in i for i in data_params['gate_img_prefix']]),
                                                    data_params=data_params,
                                                    autotune=autotune,
                                                    ext_energy_range=params['ext_energy_range']).prefetch(buffer_size=autotune)#.repeat(10000)


    if params['lr_finder']['use']:
        train_lr_finder = dpcal.tfrecord_load_data.load_dataset(filenames = train_files[:params['lr_finder']['Number_of_files']],
                                                          tag= tag, shuffle=shuffle, cache=False,
                                                          multiple_with=multiple_with, load_single_file=params['top']['BWxCB_fit'],
                                                          additional_info=True, batch_size = params['batch_size'],
                                                          cut_ratio=params['cut_ratio'],
                                                          time_lr=any(['time' in i for i in data_params['gate_img_prefix']]),
                                                          data_params=data_params,
                                                          autotune=autotune,
                                                          ext_energy_range=params['ext_energy_range']).prefetch(buffer_size=autotune)
    else:
        train_lr_finder = None
    #%%
    dirs['tensorboard'] = dirs['log']+'tensorboard/'
    if True:
        session_num = 0
        if False:
            # HP_downsampling = hp.HParam('cnn:downsampling', hp.Discrete(['maxpool' , 'avgpool'])) # , 'adam', 'Nadam'
            # HP_G_downsampling = hp.HParam('cnn:globalpooling', hp.Discrete(['maxpool' , 'avgpool', 'flatten']))
            # HP_block_depths = hp.HParam('cnn:block_depths', hp.Discrete([2,4,6]))
            # HP_number_of_blocks = hp.HParam('cnn:number_of_blocks', hp.Discrete([3,4,5]))
            # HP_number_of_units = hp.HParam('FiLM_gen:number_of_units', hp.Discrete([3,4]))
            # HP_units = hp.HParam('FiLM_gen:units', hp.Discrete([128,256,512, 1024]))
            HP_batchsize = hp.HParam('batch_size', hp.Discrete([512, 1024, 2048, 4096]))

            METRIC_ACCURACY = 'logcosh'

            with tf.summary.create_file_writer(dirs['tensorboard']).as_default():
              hp.hparams_config(
                hparams=[
                #     HP_downsampling,
                #     HP_G_downsampling,
                #     HP_block_depths,
                #     HP_number_of_blocks,
                    # HP_number_of_units,
                    # HP_units
                    HP_batchsize
                    ],
                metrics=[hp.Metric(METRIC_ACCURACY, display_name='logcosh')],
              )

            permutations = itertools.product(
                                            # HP_downsampling.domain.values,
                                            # HP_G_downsampling.domain.values,
                                            # HP_block_depths.domain.values,
                                            # HP_number_of_blocks.domain.values
                                            # HP_number_of_units.domain.values,
                                            # HP_units.domain.values
                                            HP_batchsize.domain.values
                                            )
            for batch in permutations: # permutations downsample, g_downsample, block_depths, nunber_of_blocks
                # if session_num <=1:
                #     session_num += 1
                #     continue
                print(batch)
                hparams = {
                    # HP_downsampling: downsample,
                    # HP_G_downsampling: g_downsample,
                    # HP_block_depths: block_depths,
                    # HP_number_of_blocks: nunber_of_blocks
                    # HP_number_of_units: number_of_units,
                    HP_batchsize: batch[0]
                }
                run_name = "run-%d" % session_num
                print('--- Starting trial: %s' % run_name)
                print({h.name: hparams[h] for h in hparams})
                dirs['tensorboard'] = dirs['log']+'tensorboard/'+ run_name
                run(run_dir=dirs['tensorboard'], hparams=hparams,
                    data=[train, val], params=params, dirs=dirs, save_figs=save_figs,
                    data_path=data_path, verbose=1, session_num=session_num,
                    run_same_model_x_times = run_same_model_x_times
                    )
                session_num += 1
        elif run_same_model_x_times:
            print('#'*20)
            print(f'Training {run_same_model_x_times} number of models')
            print('#'*20)
            model_names=[]
            for nr, i in enumerate(range(run_same_model_x_times)):
                _,data_params = define_and_train_model(data=[train, val], params=params, dirs=dirs.copy(),
                                                  save_figs=save_figs, data_path=data_path,
                                                  verbose=1, hparams=None, testing=False,
                                                  session_num=str(nr),
                                                  train_lr_finder=train_lr_finder)

                model_names.append(data_params['model_path'])
            print(model_names)
            for data_name in [data_params['sub_data_folder_name'].replace('train', 'test'),
                              data_params['sub_data_folder_name'].replace('mc', 'data').replace('train', 'test')]:
                str_to_run = (f"python load_model_advanced.py -g {data_params['flags']['gpu'].replace('_',',')} --data_path {data_params['data_path']} "
                             f"--gpu_dir {data_params['flags']['gpu']} --sub_data_folder_name {data_name} ")
                for i in model_names:
                    str_to_run+= f"--model_path {i} "
                print(str_to_run)
                device = cuda.get_current_device()
                device.reset()
                os.system(str_to_run)
        else:
            # fix dirs['tensorboard']
            results,data_params = define_and_train_model(data=[train, val], params=params, dirs=dirs,
                                              save_figs=save_figs, data_path=data_path,
                                              verbose=1, hparams=None, testing=False,
                                              train_lr_finder=train_lr_finder)
